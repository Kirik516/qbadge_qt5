#include <QCommandLineParser>
#include <QDebug>
#include <QDir>
#include <QGuiApplication>
#include <QPainter>
#include <QSvgGenerator>

#include "version.h"

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
  QByteArray localMsg = msg.toLocal8Bit();
  switch (type) {
  case QtDebugMsg:
      fprintf(stderr, "Debug: %s (%s:%u, %s)\n", localMsg.constData(),
              context.file, context.line, context.function);
      break;
  case QtInfoMsg:
      fprintf(stdout, "%s\n", localMsg.constData());
      break;
  case QtWarningMsg:
      fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(),
              context.file, context.line, context.function);
      break;
  case QtCriticalMsg:
      fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(),
              context.file, context.line, context.function);
      break;
  case QtFatalMsg:
      fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(),
              context.file, context.line, context.function);
      abort();
  }
}

int main(int argc, char *argv[])
{
    qInstallMessageHandler(myMessageOutput);
    QGuiApplication a(argc, argv);
    a.setApplicationName("qbadge_qt5");
#ifdef APP_VERSION
    a.setApplicationVersion(APP_VERSION);
#endif

    QCommandLineParser clParser;
    clParser.addHelpOption();

    // label text option
    QCommandLineOption label(QStringList() << "l" << "label",
                             QObject::tr("set <label-text> for label"),
                             QObject::tr("label-text"));
    clParser.addOption(label);

    // value text option
    QCommandLineOption value(QStringList() << "v" << "value",
                             QObject::tr("set <value-text> for value"),
                             QObject::tr("value-text"));
    clParser.addOption(value);

    // output file name
    QCommandLineOption out(QStringList() << "o" << "file",
                           QObject::tr("set <file> as output file"),
                           QObject::tr("file"));
    clParser.addOption(out);

    // value color
    QCommandLineOption color(QStringList() << "c" << "color",
                             QObject::tr("set value color as <color>\n"
                                         "value color in hex #AARRGGBB"),
                             QObject::tr("color"));
    clParser.addOption(color);

    // show version
    QCommandLineOption version(QStringList() << "version",
                             QObject::tr("show application version"));
    clParser.addOption(version);

    clParser.process(a);

    if (clParser.isSet(version))
    {
        clParser.showVersion();
    }

    QString fileName = clParser.value(out);
    QString labelText = clParser.value(label);
    QString valueText = clParser.value(value);
    QString valueColor = clParser.value(color);

    if (fileName.isEmpty()
            || labelText.isEmpty() || valueText.isEmpty()
            || valueColor.isEmpty())
    {
        qInfo() << "incorrect command line" << endl
                << "all options are required";
        clParser.showHelp(0);
        return 0;
    }

    QString tmpFileName("tmp");
    QSvgGenerator *svgGenTmp = new QSvgGenerator;
    svgGenTmp->setFileName(tmpFileName);

    QSize s(100, 100);
    QPainter p;
    // setting appropriate size for badge
    p.begin(svgGenTmp);
    p.setFont(QFont("Arial", 50));
    QRect labelRect = p.boundingRect(0, 0,
                                     s.width()/2 - 1, s.height(),
                                     Qt::AlignCenter, labelText);
    QRect valueRect = p.boundingRect(0, 0,
                                     s.width()/2, s.height(),
                                     Qt::AlignCenter, valueText);
    p.end();
    delete svgGenTmp;

    labelRect.setWidth(labelRect.width() + 10);
    valueRect.setWidth(valueRect.width() + 10);
    int wL = qMax(s.width()/2, labelRect.width());
    int wV = qMax(s.width()/2, valueRect.width());
    int mhL = qMax(labelRect.height(), valueRect.height());
    s.setWidth(wL + wV);
    s.setHeight(qMax(s.height(), mhL));
    labelRect.setRect(0, 0, wL, s.height());
    valueRect.setRect(wL, 0, wV, s.height());

    // applying sittings for SVG
    QSvgGenerator svgGen;
    svgGen.setFileName(fileName);
    svgGen.setTitle("test");
    svgGen.setDescription("An SVG badge for your repo created by QBadge. "
                          "Powered by Qt5.7.0.");
    svgGen.setSize(s);
    svgGen.setViewBox(QRect(0, 0, s.width(), s.height()));

    // deleting tmp file
    QDir curr = QDir::current();
    if (curr.exists(tmpFileName))
    {
        curr.remove(tmpFileName);
    }

    // making SVG file
    p.begin(&svgGen);
    // configure QPainter
    p.setFont(QFont("Arial", 50));
    p.setRenderHint(QPainter::Antialiasing);
    p.setBackground(Qt::transparent);

    // configure QBrushes
    QLinearGradient lg(0, 0, 0, s.height());
    lg.setColorAt(0, QColor("#A0A0A0"));
    lg.setColorAt(1, QColor("#505050"));
    QBrush labelBrush(lg);

    lg.setColorAt(0, QColor(valueColor).lighter());
    lg.setColorAt(1, QColor(valueColor).darker());
    QBrush valueBrush(lg);

    p.setPen(QPen(Qt::NoPen));
    // making round rects
    p.setBrush(labelBrush);
    p.drawRoundRect(0, 0, s.height(), s.height());

    p.setBrush(valueBrush);
    p.drawRoundRect(s.width() - s.height(), 0, s.height(), s.height());

    // making rects
    p.setBrush(labelBrush);
    p.drawRect(s.height()/2, 0, wL - s.height()/2, s.height());

    p.setBrush(valueBrush);
    p.drawRect(wL, 0, wV - s.height()/2, s.height());

    p.setPen(QColor("#F0F0F0"));
    p.drawText(labelRect, Qt::AlignCenter, labelText);
    p.drawText(valueRect, Qt::AlignCenter, valueText);
    p.end();

    a.exit(1);
    return 1;
}
