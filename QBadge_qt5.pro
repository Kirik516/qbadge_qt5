QT += core
QT += gui
QT += svg

CONFIG += c++11

TARGET = qbadge_qt5
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

HEADERS += \
    version.h
