app_version=$(git describe --tags --always)

echo "#ifndef VERSION_H" > version.h
echo "#define VERSION_H" >> version.h
echo >> version.h
echo "#define APP_VERSION" \"$app_version\" >> version.h
echo >> version.h
echo "#endif // VERSION_H" >> version.h
